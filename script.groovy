def buildJar() {
    echo 'building the application...'
    sh 'mvn package'
}

def buildImage() {
    echo "building the docker image..."
	withCredentials([usernamePassword(credentialsId: 'dockerhubcreds', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
		sh 'docker build -t shakthighan/prashanth:jma-21.0 .'
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh 'docker push shakthighan/prashanth:jma-21.0'
     }
}

def deployApp() {
    echo 'deploying the application...'
}

return this